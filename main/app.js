function distance(first, second){
	if(Array.isArray(first)&&Array.isArray(second)){
	var a = [], diff = [];

    for (var i = 0; i < first.length; i++) {
    	if(first[i] instanceof String){
    		 a[first[i]] = 1;
    	}
    	else{a[first[i]+70]=1;}
       
    }

    for (var i = 0; i < second.length; i++) {
    	if(second[i] instanceof String){
        if (a[second[i]]&&a[second[i]]!=2) {
           delete a[second[i]];
        } else {
            a[second[i]] = 2;
        }}
        else
        {
        	if (a[second[i]+70]&&a[second[i]+70]!=2) {
           delete a[second[i]+70];
        } else {
            a[second[i]+70] = 2;
        }
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff.length;
}
else
{
	throw new Error("InvalidType");
}
}

module.exports.distance = distance